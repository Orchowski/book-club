class Tree
  attr_accessor :childs, :node_name

  def initialize(name, childs = [])
    @childs = childs
    @node_name = name
  end

  def visit_all(&block)
    visit &block
    childs.each { |d| d.visit_all &block }
  end

  def visit(&block)
    block.call self
  end
end

tree_ruby = Tree.new("Ruby",
                     [
  Tree.new("Reia"),
  Tree.new("MacRuby"),
])

puts "Visiting node:"

tree_ruby.visit { |node| puts node.node_name }
puts "\n"

puts "Visiting all nodes:"
tree_ruby.visit_all { |node| puts node.node_name }
