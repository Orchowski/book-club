class Tree
  attr_accessor :childs, :node_name

  def initialize(opts = { :childs => [] })
    if not opts[:dict_structure].nil?
      opts[:dict_structure].each { |key, values|
          @node_name = key
          @childs = values.map{ |v,k| Tree.new(:dict_structure=>{v=>k})}
         }
    else
      @childs = opts[:childs] || []
      @node_name = opts[:name]
    end
  end

  def visit_all(&block)
    visit &block
    childs.each { |d| d.visit_all &block }
  end

  def visit(&block)
    block.call self
  end
end

tree_ruby = Tree.new({:name => "Ruby",
                     :childs =>[
  Tree.new(:name => "Reia"),
  Tree.new(:name => "MacRuby"),
]})

puts "Visiting node:"

tree_ruby.visit {|node| puts node.node_name}
puts "\n"

puts "Visiting all nodes:"
tree_ruby.visit_all {|node| puts node.node_name}

x = { "grandpa" => {
  "father" => {
    "child 1" => {},
    "child 2" => {},
  },
  "uncle" => {
    "child 3" => {},
    "child 4" => { 'grand child' =>{}},
  },
} }

tree_ruby = Tree.new :dict_structure => x

puts "Visiting node:"

tree_ruby.visit {|node| puts node.node_name}
puts "\n"

puts "Visiting all nodes:"
test_arr = []
expected_arr =[
  "grandpa",
  "father",
  "child 1",
  "child 2",
  "uncle",
  "child 3",
  "child 4",
  "grand child"
]
tree_ruby.visit_all {|node| puts node.node_name }
tree_ruby.visit_all {|node| test_arr << node.node_name }

puts "\nTest success!" if test_arr == expected_arr