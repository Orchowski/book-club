class ActsAsCsv
    class CsvRow
        def initialize(headers, values)
            @associations = Hash.new []
            headers.each_with_index do |header, i|
                @associations[header] = values[i]
            end
        end
        def method_missing(name, *args)
            @associations[name.to_s]
        end
    end
    def read
        file = File.new(self.class.to_s.downcase + '.txt')
        @headers = file.gets.chomp.split(', ')
        file.each do |row|
            @result << row.chomp.split(', ')
        end
    end

    def headers
        @headers
    end

    def csv_contents
        @result
    end
    
    def initialize
        @result = []
        read
    end

    def each(&op)
        rows = csv_contents.map {|row_arr| CsvRow.new(@headers, row_arr) }
        rows.each {|row| op.call row}
    end
end

class RubyCsv < ActsAsCsv
end
m=RubyCsv.new
m.each { |row| puts row.Col4 }
